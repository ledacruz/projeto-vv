package seleniumgluecode;

import java.util.ArrayList;
import java.util.List;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Test {
	
	@Given("^leilao estado \"([^\"]*)\"$")
	public void leilao_estado(String leilaoEstado) throws Throwable {
	    System.out.println("\nPré-condição do registar lance");
	    System.out.println("Estado do Leilão: " + leilaoEstado);
	}

	@Given("^leilao tipo \"([^\"]*)\"$")
	public void leilao_tipo(String leilaoTipo) throws Throwable {
		System.out.println("\nPré-condição do registar lance");
	    System.out.println("Tipo do Leilão: " + leilaoTipo);
	}

	@Given("^cliente tipo \"([^\"]*)\"$")
	public void cliente_tipo(String clienteTipo) throws Throwable {
		System.out.println("\nPré-condição do registar lance");
	    System.out.println("Tipo do Cliente: " + clienteTipo);
	}

	@Given("^cliente saldo (\\d+)$")
	public void cliente_saldo(int clienteSaldo) throws Throwable {
		System.out.println("\nPré-condição do registar lance");
	    System.out.println("Saldo do Cliente: " + clienteSaldo);
	}

	@When("^cliente \"([^\"]*)\" solicita a efetivacao de lance$")
	public void cliente_solicita_a_efetivacao_de_lance(String clienteNome) throws Throwable {
		System.out.println("Nome do Cliente: " + clienteNome);
	}

	@Then("^o sistema solicita o valor do lance$")
	public void o_sistema_solicita_o_valor_do_lance() throws Throwable {
		System.out.println("Sistema solicita valor do lance");
	}

	@Then("^cliente insere o valor do lance (\\d+)$")
	public void cliente_insere_o_valor_do_lance(int lanceValor) throws Throwable {
		System.out.println("Cliente insere valor do lance");
		System.out.println("Valor do lance: " + lanceValor);
	}


	@Then("^sistema valida lance (\\d+) (\\d+)$")
	public void sistema_valida_lance(int lanceValor, int clienteSaldo) throws Throwable {
		System.out.println("Sistema valida lance");
	    if (clienteSaldo < lanceValor) {
		    System.out.println("Lance Inválido");
		    throw new Exception("erro");
	    }else
	    	System.out.println("Lance válido");
	}
	
	private List<List<String>> tabelaLance = new ArrayList<List<String>>();
	

	@Given("^lance solicitado$")
	public void lance_solicitado(DataTable lance) throws Throwable {
	    for (List<String> linhaTabela : lance.raw()) {
	    	tabelaLance.add(new ArrayList<String>(linhaTabela));
	    }
	    System.out.println("\nPré condição de registar lance");
	    System.out.println("Dados do lance: " + tabelaLance.toString());
	}

	@Then("^o sistema congela valor lance (\\d+)$")
	public void o_sistema_congela_valor_lance(int valorCongelado) throws Throwable {
		System.out.println("Valor congelado: " + valorCongelado);
	}

	@Then("^registra lance \"([^\"]*)\"$")
	public void registra_lance(String lanceStatus) throws Throwable {
		System.out.println("Lance status: " + lanceStatus);
	}

	@Given("^lance registrado$")
	public void lance_registrado() throws Throwable {
	    System.out.println("Lance registrado pelos sistema.");
	}


	@Then("^sistema notifica comprador \"([^\"]*)\"$")
	public void sistema_notifica_comprador() throws Throwable {
		System.out.println("Comprador notificado com sucesso");
	}

	@Then("^sistema notifica vendedor \"([^\"]*)\"$")
	public void sistema_notifica_vendedor() throws Throwable {
		System.out.println("Vendedor notificado com sucesso");
	}

	@Given("^todas as condicoes satisfeitas$")
	public void todas_as_condicoes_satisfeitas() throws Throwable {
	    System.out.println("Todas as condições foram satisfeitas");
	}

		
	@Then("^sistema apresenta mensagem de sucesso$")
	public void sistema_apresenta_apresenta_mensagem_de_sucesso() throws Throwable {
		System.out.println("Lance realizado com sucesso!");
	}
}
