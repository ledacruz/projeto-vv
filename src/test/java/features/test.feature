#Author: ledacruz.23@gmail.com
#Feature: Caso de uso Registrar Lance

@casoDeUsoRegistarLance
Feature: Caso de uso Registrar Lance

  @solicitarLance
  Scenario Outline: Solicitar Lance
    Given leilao estado "<leilao estado>"
    And leilao tipo "<leilao tipo>"
    And cliente tipo "<cliente tipo>"
    And cliente saldo <cliente saldo>
    When cliente "<cliente nome>" solicita a efetivacao de lance
    Then o sistema solicita o valor do lance
    And cliente insere o valor do lance <lance valor>
    And sistema valida lance <lance valor> <cliente saldo>
    
    Examples: 
      | leilao estado | leilao tipo | cliente tipo | cliente saldo | cliente nome | lance valor |
      | aberto        | ingles      | comprador    | 100           | joao         | 99          |
      | aberto        | ingles      | comprador    | 100           | carmem       | 100         |
      
   @registarLance
  Scenario Outline: Registar Lance
  #pós condição de @solicitarLance
    Given lance solicitado
    | leilao estado | leilao tipo | cliente tipo | cliente saldo | cliente nome | lance valor |
    | aberto        | ingles      | comprador    | 100           | joao         | 99          |
    Then o sistema congela valor lance <valor congelado>
    And registra lance "<lance status>"
    
    Examples: 
      | valor congelado | lance status |
      | 99              | efetivado    |
      
  @notificaEnvolvidos
  Scenario Outline: Notificar envolvidos
  #pós condição de @registarLance
  	Given lance registrado
  	Then sistema notifica comprador
  	And sistema notifica vendedor
  	
      
  @lanceCriado
  Scenario: Registro de lance bem sucedido
    Given todas as condicoes satisfeitas
    Then sistema apresenta mensagem de sucesso
	